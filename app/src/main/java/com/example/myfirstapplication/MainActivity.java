package com.example.myfirstapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    EditText etfirstname,etlastname;
    TextView tvdisplay;
    Button btnok;
    ImageView imgclose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etfirstname =(EditText) findViewById(R.id.etfirstname);
        etlastname =(EditText) findViewById(R.id.etlastname);
        btnok=(Button)findViewById(R.id.btnok);
        tvdisplay=(TextView)findViewById(R.id.tvdisplay);
        imgclose=(ImageView)findViewById(R.id.imgclose);

        etlastname.getText().toString().trim();
        etlastname.getText().toString().trim();


        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String  firstname =etfirstname.getText().toString();
                String  lastname =etlastname.getText().toString();
                String temp =firstname +" "+ lastname;

                tvdisplay.setText(temp);
            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
